package wave.square.uk1104;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import jssc.SerialPortException;
import wave.square.uk1104.pin.AnalogInput;
import wave.square.uk1104.pin.DigitalInput;
import wave.square.uk1104.pin.DigitalOutput;
import wave.square.uk1104.pin.Pin;
import wave.square.uk1104.pin.TemperatureInput;
import wave.square.uk1104.pin.meterType.Linear;
import wave.square.uk1104.pin.meterType.MeterType;
import wave.square.uk1104.pin.meterType.Power;
import wave.square.uk1104.pin.meterType.Raw;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.typeadapters.RuntimeTypeAdapterFactory;

public class ReadStatusThread extends Thread {

    private Config config;
    private UK1104 kit;
    public volatile String result = "";
    private Gson gson;

    public ReadStatusThread(Config config, UK1104 kit) {
        this.config = config;
        this.kit = kit;

        RuntimeTypeAdapterFactory<Pin> pinAdapterFactory = RuntimeTypeAdapterFactory.of(Pin.class);
        pinAdapterFactory.registerSubtype(DigitalOutput.class);
        pinAdapterFactory.registerSubtype(DigitalInput.class);
        pinAdapterFactory.registerSubtype(AnalogInput.class);
        pinAdapterFactory.registerSubtype(TemperatureInput.class);

        RuntimeTypeAdapterFactory<MeterType> meterAdapterFactory = RuntimeTypeAdapterFactory.of(MeterType.class);
        meterAdapterFactory.registerSubtype(Raw.class);
        meterAdapterFactory.registerSubtype(Linear.class);
        meterAdapterFactory.registerSubtype(Power.class);

        gson = new GsonBuilder().setPrettyPrinting().registerTypeAdapterFactory(pinAdapterFactory).registerTypeAdapterFactory(meterAdapterFactory).excludeFieldsWithModifiers(Modifier.STATIC).setExclusionStrategies(new PasswordExclusionStrategy()).create();
    }

    @Override
    public void run() {
        while (!isInterrupted()) {
            try {
                boolean[] relayStatuses = kit.getRelayStatuses();
                config.relays.get(0).on = relayStatuses[0];
                config.relays.get(1).on = relayStatuses[1];
                config.relays.get(2).on = relayStatuses[2];
                config.relays.get(3).on = relayStatuses[3];
                for (int x = 0; x < 6; x++) {
                    Pin p = config.pins.get(x);
                    if (p.disabled) {
                        continue;
                    }
                    if (p instanceof AnalogInput) {
                        AnalogInput ai = (AnalogInput) p;
                        ai.value = kit.getAnalogChannel(x);
                        List<Double> intervals = new ArrayList<Double>(ai.intervals);
                        intervals.add(ai.value);
                        Collections.sort(intervals);
                        int index = intervals.indexOf(ai.value);
                        if (index >= ai.intervalColors.size()) {
                            index = ai.intervalColors.size() - 1;
                        }
                        ai.needleColor = ai.intervalColors.get(index);
                    } else if (p instanceof DigitalInput) {
                        ((DigitalInput) p).on = kit.getDigitalChannel(x);
                    } else if (p instanceof TemperatureInput) {
                        TemperatureInput ti = (TemperatureInput) p;
                        ti.value = kit.getTempChannel(x) + ((TemperatureInput) p).calibration;
                        List<Double> intervals = new ArrayList<Double>(ti.intervals);
                        intervals.add(ti.value);
                        Collections.sort(intervals);
                        int index = intervals.indexOf(ti.value);
                        if (index >= ti.intervalColors.size()) {
                            index = ti.intervalColors.size() - 1;
                        }
                        ti.needleColor = ti.intervalColors.get(index);
                    } else if (p instanceof DigitalOutput) {
                        ((DigitalOutput) p).on = kit.getDigitalChannel(x);
                    }
                }
                result = gson.toJson(config);
            } catch (SerialPortException e) {
                e.printStackTrace();
            }
        }
    }

    private class PasswordExclusionStrategy implements ExclusionStrategy {

        @Override
        public boolean shouldSkipClass(Class<?> clazz) {
            return false;
        }

        @Override
        public boolean shouldSkipField(FieldAttributes f) {
            return f.getAnnotation(Sensitive.class) != null;
        }

    }

}