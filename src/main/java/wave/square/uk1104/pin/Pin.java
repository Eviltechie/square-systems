package wave.square.uk1104.pin;

import wave.square.uk1104.UK1104.ChannelMode;

public abstract class Pin {

    String label = "Label";
    public boolean disabled = false;

    public abstract ChannelMode getChannelMode();

}