package wave.square.uk1104.pin;

import wave.square.uk1104.UK1104.ChannelMode;

public class DigitalInput extends Pin {

    enum Color {
        green,
        yellow,
        red,
        off,
    }

    public String offLabel = "Off";
    public Color offColor = Color.red;
    public String onLabel = "On";
    public Color onColor = Color.green;
    public transient boolean on = false;

    @Override
    public ChannelMode getChannelMode() {
        return ChannelMode.DIGITAL_IN;
    }

}