package wave.square.uk1104.pin;

import java.util.ArrayList;
import java.util.List;

import wave.square.uk1104.UK1104.ChannelMode;
import wave.square.uk1104.pin.meterType.Linear;
import wave.square.uk1104.pin.meterType.MeterType;

public class AnalogInput extends Pin {

    public AnalogInput() {
        intervals.add(10.0);
        intervals.add(20.0);
        intervals.add(30.0);
        intervals.add(40.0);
        intervals.add(50.0);

        intervalColors.add("#cc6666");
        intervalColors.add("#e7e658");
        intervalColors.add("#66cc66");
        intervalColors.add("#e7e658");
        intervalColors.add("#cc6666");
    }

    public MeterType meterType = new Linear();
    public String unit = "V";
    public double min = 0;
    public double max = 50;
    public List<Double> intervals = new ArrayList<Double>();
    public List<String> intervalColors = new ArrayList<String>();
    public transient String needleColor = "#000000";
    public double calibration = 1;
    public transient double value = 0;

    @Override
    public ChannelMode getChannelMode() {
        return ChannelMode.ANALOG_IN;
    }

}