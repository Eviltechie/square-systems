package wave.square.uk1104.pin.meterType;

public class Linear implements MeterType {

    @Override
    public double calculate(int raw, double calibration) {
        return raw * (5.0 / 1024) * calibration;
    }

}