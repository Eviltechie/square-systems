package wave.square.uk1104.pin.meterType;

public class Power implements MeterType {

    @Override
    public double calculate(int raw, double calibration) {
        return Math.pow(raw * (5.0 / 1024), 2) * calibration;
    }

}