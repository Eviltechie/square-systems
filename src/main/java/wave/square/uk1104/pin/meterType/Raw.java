package wave.square.uk1104.pin.meterType;

public class Raw implements MeterType {

    @Override
    public double calculate(int raw, double calibration) {
        return raw;
    }

}