package wave.square.uk1104.pin.meterType;

public interface MeterType {

    public double calculate(int raw, double calibration);

}