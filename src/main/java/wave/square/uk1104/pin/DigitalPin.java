package wave.square.uk1104.pin;

public abstract class DigitalPin extends Pin {

    public String offLabel = "Off";
    public String onLabel = "On";

}