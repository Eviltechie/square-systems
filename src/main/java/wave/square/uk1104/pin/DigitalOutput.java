package wave.square.uk1104.pin;

import wave.square.uk1104.UK1104.ChannelMode;

public class DigitalOutput extends DigitalPin {

    public String pulseLabel = "Pulse";
    public double pulseTime = 1;
    public transient boolean on;

    @Override
    public ChannelMode getChannelMode() {
        return ChannelMode.DIGITAL_OUT;
    }

}