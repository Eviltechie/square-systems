package wave.square.uk1104.pin;

public class Relay {

    public String label = "Relay";
    public String offLabel = "Off";
    public String onLabel = "On";
    public String pulseLabel = "Pulse";
    public double pulseTime = 1;
    public boolean disabled = false;
    public transient boolean on; //True if the relay is on

}
