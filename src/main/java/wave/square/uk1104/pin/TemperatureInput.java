package wave.square.uk1104.pin;

import java.util.ArrayList;
import java.util.List;

import wave.square.uk1104.UK1104.ChannelMode;

public class TemperatureInput extends Pin {

    public TemperatureInput() {
        intervals.add(15.0);
        intervals.add(18.0);
        intervals.add(26.0);
        intervals.add(30.0);
        intervals.add(125.0);

        intervalColors.add("#cc6666");
        intervalColors.add("#e7e658");
        intervalColors.add("#66cc66");
        intervalColors.add("#e7e658");
        intervalColors.add("#cc6666");
    }

    public String unit = "&deg;C";
    public double min = -55;
    public double max = 125;
    public List<Double> intervals = new ArrayList<Double>();
    public List<String> intervalColors = new ArrayList<String>();
    public transient String needleColor = "#000000";
    public double calibration = 1;
    public transient double value = 0;

    @Override
    public ChannelMode getChannelMode() {
        return ChannelMode.TEMPERATURE;
    }

}