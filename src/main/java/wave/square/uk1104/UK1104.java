package wave.square.uk1104;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jssc.SerialPort;
import jssc.SerialPortException;
import jssc.SerialPortTimeoutException;

import org.apache.commons.lang3.Validate;

public class UK1104 {

    public enum ChannelMode {
        DIGITAL_OUT(1),
        DIGITAL_IN(2),
        ANALOG_IN(3),
        TEMPERATURE(4);

        private int modeValue;

        private ChannelMode(int val) {
            modeValue = val;
        }

        public int getModeValue() {
            return modeValue;
        }
    }

    private SerialPort serialPort;

    public UK1104(Config config) throws SerialPortException {
        serialPort = new SerialPort(config.serialPort);
        if (!serialPort.openPort()) {
            throw new RuntimeException("Serial port " + config.serialPort + " failed to open!");
        }
        serialPort.setParams(9600, 8, 1, 0);
        serialPort.writeString("\r\n");
        receiveResult();

        for (int x = 0; x < 6; x++) {
            setChannelMode(x, config.pins.get(x).getChannelMode());
        }
        setTempResolution(config.temperatureResolution);
    }

    private synchronized String receiveResult() throws SerialPortException {
        String result;
        try {
            result = serialPort.readString(1, 2000);
            while (!result.contains("::")) {
                result += serialPort.readString(1, 2000);
            }
        } catch (SerialPortTimeoutException e) {
            result = "";
        }
        return result;
    }

    private synchronized String sendCommand(String command) throws SerialPortException {
        serialPort.writeString(command);
        String result = receiveResult();
        while (!result.contains(command)) {
            serialPort.writeString("\r\n\r\n\r\n");
            try {
                Thread.sleep(1000);
                while (serialPort.readBytes() != null) {
                    Thread.sleep(100);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            serialPort.writeString(command);
            result = receiveResult();
        }
        return result;
    }

    public synchronized void setRelay(int relay, boolean status) throws SerialPortException {
        relay++;
        Validate.inclusiveBetween(1, 4, relay);
        if (status) {
            relayOn(relay);
        } else {
            relayOff(relay);
        }
    }

    private synchronized void relayOn(int relay) throws SerialPortException {
        Validate.inclusiveBetween(1, 4, relay);
        sendCommand(String.format("REL%s.ON\r\n", relay));
    }

    private synchronized void relayOff(int relay) throws SerialPortException {
        Validate.inclusiveBetween(1, 4, relay);
        sendCommand(String.format("REL%s.OFF\r\n", relay));
    }

    public synchronized void toggleRelay(int relay) throws SerialPortException {
        Validate.inclusiveBetween(0, 3, relay);
        setRelay(relay, !getRelayStatus(relay));
    }

    public synchronized boolean getRelayStatus(int relay) throws SerialPortException {
        relay++;
        Validate.inclusiveBetween(1, 4, relay);
        while (true) {
            String result = sendCommand(String.format("REL%s.GET\r\n", relay));
            Pattern pattern = Pattern.compile("(\\d)\\s*?::");
            Matcher matcher = pattern.matcher(result);
            if (matcher.find()) {
                if (matcher.group(1).equals("1")) {
                    return true;
                } else {
                    return false;
                }
            }
        }
    }

    public synchronized void setRelaysOn() throws SerialPortException {
        sendCommand("RELS.ON\r\n");
    }

    public synchronized void setRelaysOff() throws SerialPortException {
        sendCommand("RELS.OFF\r\n");
    }

    public synchronized boolean[] getRelayStatuses() throws SerialPortException {
        while (true) {
            boolean[] statuses = new boolean[4];
            String result = sendCommand("RELS.GET\r\n");
            Pattern pattern = Pattern.compile("(\\d) (\\d) (\\d) (\\d)\\s*?::");
            Matcher matcher = pattern.matcher(result);
            if (matcher.find()) {
                for (int x = 1; x < 5; x++) {
                    if (matcher.group(x).equals("1")) {
                        statuses[x - 1] = true;
                    } else {
                        statuses[x - 1] = false;
                    }
                }
                return statuses;
            }
        }
    }

    public synchronized void setChannel(int channel, boolean status) throws SerialPortException {
        channel++;
        Validate.inclusiveBetween(1, 6, channel);
        if (status) {
            setChannelOn(channel);
        } else {
            setChannelOff(channel);
        }
    }

    private synchronized void setChannelOn(int channel) throws SerialPortException {
        Validate.inclusiveBetween(1, 6, channel);
        sendCommand(String.format("CH%s.ON\r\n", channel));
    }

    private synchronized void setChannelOff(int channel) throws SerialPortException {
        Validate.inclusiveBetween(1, 6, channel);
        sendCommand(String.format("CH%s.OFF\r\n", channel));
    }

    public synchronized void toggleChannel(int channel) throws SerialPortException {
        Validate.inclusiveBetween(0, 5, channel);
        setChannel(channel, !getDigitalChannel(channel));
    }

    public synchronized boolean getDigitalChannel(int channel) throws SerialPortException {
        channel++;
        Validate.inclusiveBetween(1, 6, channel);
        while (true) {
            String result = sendCommand(String.format("CH%s.GET\r\n", channel));
            Pattern pattern = Pattern.compile("(\\d)\\s*?::");
            Matcher matcher = pattern.matcher(result);
            if (matcher.find()) {
                if (matcher.group(1).equals("1")) {
                    return true;
                } else {
                    return false;
                }
            }
        }
    }

    public synchronized void setChannelMode(int channel, ChannelMode mode) throws SerialPortException {
        channel++;
        Validate.inclusiveBetween(1, 6, channel);
        Validate.notNull(mode);
        sendCommand(String.format("CH%s.SETMODE(%s)\r\n", channel, mode.getModeValue()));
    }

    public synchronized double getAnalogChannel(int channel) throws SerialPortException {
        channel++;
        Validate.inclusiveBetween(1, 6, channel);
        while (true) {
            String result = sendCommand(String.format("CH%s.GETANALOG\r\n", channel));
            Pattern pattern = Pattern.compile("(\\d+)\\s*?::");
            Matcher matcher = pattern.matcher(result);
            if (matcher.find()) {
                return Double.parseDouble(matcher.group(1));
            } else {
                return -1;
            }
        }
    }

    public synchronized double getTempChannel(int channel) throws SerialPortException {
        channel++;
        Validate.inclusiveBetween(1, 6, channel);
        while (true) {
            String result = sendCommand(String.format("CH%s.GETTEMP\r\n", channel));
            Pattern pattern = Pattern.compile("(\\d+\\.\\d+)\\s*?::");
            Matcher matcher = pattern.matcher(result);
            if (matcher.find()) {
                return Double.parseDouble(matcher.group(1));
            } else {
                return -1;
            }
        }
    }

    public synchronized void setChannelsOn() throws SerialPortException {
        sendCommand("CHS.ON\r\n");
    }

    public synchronized void setChannelsOff() throws SerialPortException {
        sendCommand("CHS.OFF\r\n");
    }

    public synchronized boolean[] getChannels() throws SerialPortException {
        boolean[] statuses = new boolean[6];
        while (true) {
            String result = sendCommand("CHS.GET\r\n");
            Pattern pattern = Pattern.compile("(\\d) (\\d) (\\d) (\\d) (\\d) (\\d)\\s*?::");
            Matcher matcher = pattern.matcher(result);
            if (matcher.find()) {
                for (int x = 1; x < 7; x++) {
                    if (matcher.group(x).equals("1")) {
                        statuses[x - 1] = true;
                    } else {
                        statuses[x - 1] = false;
                    }
                }
                return statuses;
            }
        }
    }

    public synchronized void setTempResolution(int resolution) throws SerialPortException {
        Validate.inclusiveBetween(9, 12, resolution);
        sendCommand(String.format("CHS.SETTEMPRES(%s)\r\n", resolution));
    }

    public synchronized void setID(String id) throws SerialPortException {
        Validate.validIndex(id, 1);
        sendCommand(String.format("SETID(%s)\r\n", id.substring(0, 2)));
    }

    public synchronized String about() throws SerialPortException {
        return sendCommand("ABOUT\r\n");
    }

    public synchronized void close() {
        try {
            serialPort.closePort();
        } catch (SerialPortException e) {
            e.printStackTrace();
        }
    }

}