package wave.square.uk1104;

import jssc.SerialPortException;
import spark.Request;
import spark.Response;
import spark.Route;

public class TriggerRelayRoute implements Route {

    private Config config;
    private UK1104 kit;

    public TriggerRelayRoute(Config config, UK1104 kit) {
        this.config = config;
        this.kit = kit;
    }

    @Override
    public Object handle(Request request, Response response) {
        try {
            int relay = Integer.parseInt(request.params("number"));
            if (config.relays.get(relay).disabled) {
                return "";
            }
            if (request.params("action").equalsIgnoreCase("on")) {
                kit.setRelay(relay, true);
            } else if (request.params("action").equalsIgnoreCase("off")) {
                kit.setRelay(relay, false);
            } else if (request.params("action").equalsIgnoreCase("pulse")) {
                kit.toggleRelay(relay);
                Thread.sleep((long) (config.relays.get(relay).pulseTime * 1000));
                kit.toggleRelay(relay);
            }
        } catch (SerialPortException | InterruptedException | NumberFormatException e) {
            e.printStackTrace();
        }
        return "";
    }
}