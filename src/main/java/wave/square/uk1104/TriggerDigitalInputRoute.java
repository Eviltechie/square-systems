package wave.square.uk1104;

import jssc.SerialPortException;
import spark.Request;
import spark.Response;
import spark.Route;
import wave.square.uk1104.pin.DigitalOutput;

public class TriggerDigitalInputRoute implements Route {

    private Config config;
    private UK1104 kit;

    public TriggerDigitalInputRoute(Config config, UK1104 kit) {
        this.config = config;
        this.kit = kit;
    }

    @Override
    public Object handle(Request request, Response response) {
        try {
            int pin = Integer.parseInt(request.params("number"));
            if (config.pins.get(pin).disabled) {
                return "";
            }
            if (request.params("action").equalsIgnoreCase("on")) {
                kit.setChannel(pin, true);
            } else if (request.params("action").equalsIgnoreCase("off")) {
                kit.setChannel(pin, false);
            } else if (request.params("action").equalsIgnoreCase("pulse")) {
                kit.toggleChannel(pin);
                Thread.sleep((long) ((DigitalOutput) config.pins.get(pin)).pulseTime * 1000);
                kit.toggleChannel(pin);
            }
        } catch (SerialPortException | InterruptedException | NumberFormatException e) {
            e.printStackTrace();
        }
        return "";
    }
}