package wave.square.uk1104;

import java.util.ArrayList;
import java.util.List;

import wave.square.uk1104.pin.AnalogInput;
import wave.square.uk1104.pin.DigitalInput;
import wave.square.uk1104.pin.DigitalOutput;
import wave.square.uk1104.pin.Pin;
import wave.square.uk1104.pin.Relay;
import wave.square.uk1104.pin.TemperatureInput;

public class Config {

    public Config() {
        for (int x = 0; x < 4; x++) {
            relays.add(new Relay());
        }
        pins.add(new AnalogInput());
        pins.add(new DigitalInput());
        pins.add(new TemperatureInput());
        pins.add(new DigitalOutput());
        pins.add(new DigitalOutput());
        pins.add(new DigitalOutput());
    }

    public String title = "UK1104 Relay Control";

    public String serialPort = "COM1";

    public int serverPort = 4567;

    @Sensitive
    public String operatorUsername = "admin";
    @Sensitive
    public String operatorPassword = "password";

    @Sensitive
    public String logUsername = "log";
    @Sensitive
    public String logPassword = "password";

    public int temperatureResolution = 9;

    public List<Relay> relays = new ArrayList<Relay>();

    public List<Pin> pins = new ArrayList<Pin>();

}