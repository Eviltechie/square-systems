package wave.square.uk1104;

import spark.Request;
import spark.Response;
import spark.Route;

public class ReadStatusRoute implements Route {

    private ReadStatusThread statusThread;

    public ReadStatusRoute(Config config, UK1104 kit) {
        statusThread = new ReadStatusThread(config, kit);
        statusThread.start();
    }

    @Override
    public Object handle(Request request, Response response) {
        try {
            return statusThread.result;
        } catch (Exception e) {
            return "error";
        }
    }

}