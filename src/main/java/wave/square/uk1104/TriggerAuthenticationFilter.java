package wave.square.uk1104;

import static spark.Spark.halt;
import spark.Filter;
import spark.Request;
import spark.Response;
import spark.Session;

public class TriggerAuthenticationFilter implements Filter {

    @Override
    public void handle(Request request, Response response) throws Exception {
        Session session = request.session();
        if (session.attribute("admin") != null && session.attribute("admin").equals(true)) {
            return;
        } else {
            halt(403, "Forbidden");
        }
    }

}