package wave.square.uk1104;

import static spark.Spark.halt;

import org.apache.commons.codec.binary.Base64;

import spark.Filter;
import spark.Request;
import spark.Response;
import spark.Session;

public class AuthenticationFilter implements Filter {

    private Config config;

    public AuthenticationFilter(Config config) {
        this.config = config;
    }

    @Override
    public void handle(Request request, Response response) throws Exception {
        Session session = request.session();
        session.maxInactiveInterval(3600);
        if (session.attribute("loggedIn") != null && session.attribute("loggedIn").equals(true)) {
            return;
        } else if (request.headers("Authorization") != null) {
            String userPass = new String(Base64.decodeBase64(request.headers("Authorization").substring(6)));
            if (userPass.split(":").length > 1) {
                String username = userPass.split(":")[0];
                String password = userPass.split(":")[1];
                if (username.equalsIgnoreCase(config.operatorUsername) && password.equals(config.operatorPassword)) {
                    session.attribute("loggedIn", true);
                    session.attribute("admin", true);
                    return;
                } else if (username.equalsIgnoreCase(config.logUsername) && password.equals(config.logPassword)) {
                    session.attribute("loggedIn", true);
                    session.attribute("admin", false);
                    return;
                }
            }
            response.header("WWW-Authenticate", "Basic");
            halt(401, "Unauthorized");
        } else {
            response.header("WWW-Authenticate", "Basic");
            halt(401, "Unauthorized");
            return;
        }
    }

}