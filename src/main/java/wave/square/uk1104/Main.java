package wave.square.uk1104;

import static spark.Spark.before;
import static spark.Spark.get;
import static spark.SparkBase.setPort;
import static spark.SparkBase.staticFileLocation;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import jssc.SerialPortException;
import wave.square.uk1104.pin.AnalogInput;
import wave.square.uk1104.pin.DigitalInput;
import wave.square.uk1104.pin.DigitalOutput;
import wave.square.uk1104.pin.Pin;
import wave.square.uk1104.pin.TemperatureInput;
import wave.square.uk1104.pin.meterType.Linear;
import wave.square.uk1104.pin.meterType.MeterType;
import wave.square.uk1104.pin.meterType.Power;
import wave.square.uk1104.pin.meterType.Raw;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.typeadapters.RuntimeTypeAdapterFactory;

public class Main {

    public static void main(String[] args) throws SerialPortException, JsonIOException, IOException {
        RuntimeTypeAdapterFactory<Pin> pinAdapterFactory = RuntimeTypeAdapterFactory.of(Pin.class);
        pinAdapterFactory.registerSubtype(DigitalOutput.class);
        pinAdapterFactory.registerSubtype(DigitalInput.class);
        pinAdapterFactory.registerSubtype(AnalogInput.class);
        pinAdapterFactory.registerSubtype(TemperatureInput.class);

        RuntimeTypeAdapterFactory<MeterType> meterAdapterFactory = RuntimeTypeAdapterFactory.of(MeterType.class);
        meterAdapterFactory.registerSubtype(Raw.class);
        meterAdapterFactory.registerSubtype(Linear.class);
        meterAdapterFactory.registerSubtype(Power.class);

        Gson gson = new GsonBuilder().setPrettyPrinting().registerTypeAdapterFactory(pinAdapterFactory).registerTypeAdapterFactory(meterAdapterFactory).create();

        Config config;

        File configFile = new File("config.json");
        if (configFile.exists()) {
            config = gson.fromJson(new FileReader(configFile), Config.class);
        } else {
            config = new Config();
            FileWriter writer = new FileWriter(configFile);
            gson.toJson(config, writer);
            writer.close();
        }

        UK1104 kit = new UK1104(config);

        setPort(config.serverPort);

        staticFileLocation("/static");

        before("/*", new AuthenticationFilter(config));

        before("/trigger/*", new TriggerAuthenticationFilter());

        get("/read", new ReadStatusRoute(config, kit));

        get("/trigger/relay/:number/:action", new TriggerRelayRoute(config, kit));

        get("/trigger/digital-input/:number/:action", new TriggerDigitalInputRoute(config, kit));
    }
}